--
-- PostgreSQL database dump
--

-- Dumped from database version 14.0
-- Dumped by pg_dump version 14.0

-- Started on 2021-11-01 16:42:04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 209 (class 1259 OID 18159)
-- Name: access_card_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.access_card_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.access_card_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 210 (class 1259 OID 18160)
-- Name: access_card; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.access_card (
    idaccess_card integer DEFAULT nextval('public.access_card_seq'::regclass) NOT NULL,
    cardnumber character varying(64),
    validaity date
);


ALTER TABLE public.access_card OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 18247)
-- Name: access_history_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.access_history_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.access_history_seq OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 18248)
-- Name: access_history; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.access_history (
    idaccess_history integer DEFAULT nextval('public.access_history_seq'::regclass) NOT NULL,
    door_iddoor integer NOT NULL,
    access_card_idaccess_card integer NOT NULL,
    date timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.access_history OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 18166)
-- Name: address_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.address_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.address_seq OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 18167)
-- Name: address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.address (
    idaddress integer DEFAULT nextval('public.address_seq'::regclass) NOT NULL,
    city character varying(45),
    street character varying(45),
    number integer,
    zipcode integer,
    street_number integer
);


ALTER TABLE public.address OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 18211)
-- Name: allowed_access_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.allowed_access_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.allowed_access_seq OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 18212)
-- Name: allowed_access; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.allowed_access (
    idallowed_access integer DEFAULT nextval('public.allowed_access_seq'::regclass) NOT NULL,
    access_card_idaccess_card integer NOT NULL,
    door_iddoor integer NOT NULL
);


ALTER TABLE public.allowed_access OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 18204)
-- Name: door_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.door_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.door_seq OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 18205)
-- Name: door; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.door (
    iddoor integer DEFAULT nextval('public.door_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    floor integer NOT NULL,
    room integer NOT NULL
);


ALTER TABLE public.door OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 18288)
-- Name: ehe_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ehe_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ehe_seq OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 18173)
-- Name: employees_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.employees_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.employees_seq OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 18174)
-- Name: employees; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.employees (
    iduser integer DEFAULT nextval('public.employees_seq'::regclass) NOT NULL,
    firstname character varying(45) NOT NULL,
    lastname character varying(45) NOT NULL,
    birthday date,
    access_card_idaccess_card integer NOT NULL,
    email character varying(45) NOT NULL,
    telephone character varying(45),
    department character varying(45) NOT NULL,
    function character varying(45) NOT NULL,
    address_idaddress integer NOT NULL
);


ALTER TABLE public.employees OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 18289)
-- Name: employees_has_events; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.employees_has_events (
    employees_iduser integer DEFAULT nextval('public.ehe_seq'::regclass) NOT NULL,
    events_idevents integer NOT NULL
);


ALTER TABLE public.employees_has_events OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 18276)
-- Name: events_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.events_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.events_seq OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 18277)
-- Name: events; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.events (
    idevents integer DEFAULT nextval('public.events_seq'::regclass) NOT NULL,
    eventb timestamp(0) without time zone,
    evente timestamp(0) without time zone,
    important character varying(45),
    title character varying(45),
    description character varying(255),
    eventadmin integer
);


ALTER TABLE public.events OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 18264)
-- Name: login_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.login_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.login_seq OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 18265)
-- Name: login; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.login (
    idlogin integer DEFAULT nextval('public.login_seq'::regclass) NOT NULL,
    employees_iduser integer NOT NULL,
    login character varying(45),
    password character varying(45)
);


ALTER TABLE public.login OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 18228)
-- Name: message_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.message_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.message_seq OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 18229)
-- Name: message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.message (
    idmessage integer DEFAULT nextval('public.message_seq'::regclass) NOT NULL,
    employees_iduser_from integer NOT NULL,
    employees_iduser_to integer NOT NULL,
    text text NOT NULL,
    date timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.message OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 18190)
-- Name: note_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.note_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.note_seq OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 18191)
-- Name: note; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.note (
    idnote integer DEFAULT nextval('public.note_seq'::regclass) NOT NULL,
    employees_iduser integer NOT NULL,
    messtext text
);


ALTER TABLE public.note OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 18305)
-- Name: pay_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pay_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pay_seq OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 18306)
-- Name: pay; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pay (
    idpay integer DEFAULT nextval('public.pay_seq'::regclass) NOT NULL,
    employees_iduser integer NOT NULL,
    grosswage integer,
    rewards integer,
    hoursworked integer,
    carriedout timestamp(0) without time zone
);


ALTER TABLE public.pay OWNER TO postgres;

--
-- TOC entry 3232 (class 2606 OID 18165)
-- Name: access_card access_card_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.access_card
    ADD CONSTRAINT access_card_pkey PRIMARY KEY (idaccess_card);


--
-- TOC entry 3246 (class 2606 OID 18253)
-- Name: access_history access_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.access_history
    ADD CONSTRAINT access_history_pkey PRIMARY KEY (idaccess_history);


--
-- TOC entry 3234 (class 2606 OID 18172)
-- Name: address address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_pkey PRIMARY KEY (idaddress);


--
-- TOC entry 3242 (class 2606 OID 18217)
-- Name: allowed_access allowed_access_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.allowed_access
    ADD CONSTRAINT allowed_access_pkey PRIMARY KEY (idallowed_access);


--
-- TOC entry 3240 (class 2606 OID 18210)
-- Name: door door_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.door
    ADD CONSTRAINT door_pkey PRIMARY KEY (iddoor);


--
-- TOC entry 3252 (class 2606 OID 18294)
-- Name: employees_has_events employees_has_events_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.employees_has_events
    ADD CONSTRAINT employees_has_events_pkey PRIMARY KEY (employees_iduser, events_idevents);


--
-- TOC entry 3236 (class 2606 OID 18179)
-- Name: employees employees_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.employees
    ADD CONSTRAINT employees_pkey PRIMARY KEY (iduser);


--
-- TOC entry 3250 (class 2606 OID 18282)
-- Name: events events_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_pkey PRIMARY KEY (idevents);


--
-- TOC entry 3248 (class 2606 OID 18270)
-- Name: login login_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.login
    ADD CONSTRAINT login_pkey PRIMARY KEY (idlogin);


--
-- TOC entry 3244 (class 2606 OID 18236)
-- Name: message message_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_pkey PRIMARY KEY (idmessage);


--
-- TOC entry 3238 (class 2606 OID 18198)
-- Name: note note_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.note
    ADD CONSTRAINT note_pkey PRIMARY KEY (idnote);


--
-- TOC entry 3254 (class 2606 OID 18311)
-- Name: pay pay_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pay
    ADD CONSTRAINT pay_pkey PRIMARY KEY (idpay);


--
-- TOC entry 3262 (class 2606 OID 18254)
-- Name: access_history fk_access_history_access_card1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.access_history
    ADD CONSTRAINT fk_access_history_access_card1 FOREIGN KEY (access_card_idaccess_card) REFERENCES public.access_card(idaccess_card);


--
-- TOC entry 3263 (class 2606 OID 18259)
-- Name: access_history fk_access_history_door1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.access_history
    ADD CONSTRAINT fk_access_history_door1 FOREIGN KEY (door_iddoor) REFERENCES public.door(iddoor);


--
-- TOC entry 3258 (class 2606 OID 18218)
-- Name: allowed_access fk_allowed_access_access_card1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.allowed_access
    ADD CONSTRAINT fk_allowed_access_access_card1 FOREIGN KEY (access_card_idaccess_card) REFERENCES public.access_card(idaccess_card);


--
-- TOC entry 3259 (class 2606 OID 18223)
-- Name: allowed_access fk_allowed_access_door1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.allowed_access
    ADD CONSTRAINT fk_allowed_access_door1 FOREIGN KEY (door_iddoor) REFERENCES public.door(iddoor);


--
-- TOC entry 3255 (class 2606 OID 18180)
-- Name: employees fk_employees_access_card1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.employees
    ADD CONSTRAINT fk_employees_access_card1 FOREIGN KEY (access_card_idaccess_card) REFERENCES public.access_card(idaccess_card);


--
-- TOC entry 3256 (class 2606 OID 18185)
-- Name: employees fk_employees_address1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.employees
    ADD CONSTRAINT fk_employees_address1 FOREIGN KEY (address_idaddress) REFERENCES public.address(idaddress);


--
-- TOC entry 3266 (class 2606 OID 18295)
-- Name: employees_has_events fk_employees_has_events_employees1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.employees_has_events
    ADD CONSTRAINT fk_employees_has_events_employees1 FOREIGN KEY (employees_iduser) REFERENCES public.employees(iduser);


--
-- TOC entry 3267 (class 2606 OID 18300)
-- Name: employees_has_events fk_employees_has_events_events1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.employees_has_events
    ADD CONSTRAINT fk_employees_has_events_events1 FOREIGN KEY (events_idevents) REFERENCES public.events(idevents);


--
-- TOC entry 3264 (class 2606 OID 18271)
-- Name: login fk_login_employees1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.login
    ADD CONSTRAINT fk_login_employees1 FOREIGN KEY (employees_iduser) REFERENCES public.employees(iduser);


--
-- TOC entry 3260 (class 2606 OID 18237)
-- Name: message fk_message_employees1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT fk_message_employees1 FOREIGN KEY (employees_iduser_from) REFERENCES public.employees(iduser);


--
-- TOC entry 3261 (class 2606 OID 18242)
-- Name: message fk_message_employees2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT fk_message_employees2 FOREIGN KEY (employees_iduser_to) REFERENCES public.employees(iduser);


--
-- TOC entry 3257 (class 2606 OID 18199)
-- Name: note fk_note_employees; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.note
    ADD CONSTRAINT fk_note_employees FOREIGN KEY (employees_iduser) REFERENCES public.employees(iduser);


--
-- TOC entry 3268 (class 2606 OID 18312)
-- Name: pay fk_pay_employees1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pay
    ADD CONSTRAINT fk_pay_employees1 FOREIGN KEY (employees_iduser) REFERENCES public.employees(iduser);


--
-- TOC entry 3265 (class 2606 OID 18283)
-- Name: events foreginkey_eventadmin; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT foreginkey_eventadmin FOREIGN KEY (eventadmin) REFERENCES public.employees(iduser);


-- Completed on 2021-11-01 16:42:06

--
-- PostgreSQL database dump complete
--

