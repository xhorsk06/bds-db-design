import os
import string
import random

def generate(random_chars):
    alphabet="0123456789abcdef"
    r = random.SystemRandom()
    return ''.join([r.choice(alphabet) for i in range(random_chars)])

def generatetext(random_chars):
    alphabet="abcdefg      hijklmn  opqrstuvyz      oeaioeai"
    r = random.SystemRandom()
    return ''.join([r.choice(alphabet) for i in range(random_chars)])

def selectR():
    rooms=("kancelář", "sklad", "dílna", "laboratoř", "kancelář", "jednací místnost", "jídelna", "kancelář", "kancelář")
    r = random.SystemRandom()
    return r.choice(rooms)

def selectS():
    rooms=("Pod Vodárnou", "Polní", "Jihlavská", "U Školy", "Kostelní")
    r = random.SystemRandom()
    return r.choice(rooms)


for n in range(50):
    print("INSERT INTO public.access_card(cardnumber, validaity)Values('" + str(generate(24))+"', '"+str(random.randrange(1,29))+'.'+str(random.randrange(1,13))+'.'+str(random.randrange(2022,2030))+"');")

for n in range(50):
    print("INSERT INTO public.door(name, floor, room)Values('" + str(selectR())+"', "+str(random.randrange(1,30))+', '+str(random.randrange(1,70))+");")

for n in range(5):
    print("INSERT INTO public.address(city, street, number, zipcode, street_number)Values('Polná', '" + str(selectS())+"', "+str(random.randrange(1,2310))+', 58813, '+str(random.randrange(1,70))+");")

for n in range(10): #access
    print("INSERT INTO public.allowed_access(access_card_idaccess_card, door_iddoor)Values("+str(random.randrange(1,50))+', '+str(random.randrange(1,50))+");")

def selectFN():
    ch=("Petr", "Lukáš", "Marek", "Jan")
    r = random.SystemRandom()
    return r.choice(ch)

def selectLN():
    ch=("Cina", "Kučera", "Rychlý", "Sedlák", "Mokrý", "Zelinka", "Němec")
    r = random.SystemRandom()
    return r.choice(ch)

def selectFC():
    ch=("ředitel", "mistr", "pracovník", "bezpečnostní technik")
    r = random.SystemRandom()
    return r.choice(ch)

for n in range(5): #empl
    fn = selectFN()
    ln =  selectLN()
    email = fn + ln + str(n) + "@jobs.cz"
    br = str(random.randrange(1,29))+'.'+str(random.randrange(1,13))+'.'+str(random.randrange(1950,2001))
    print("INSERT INTO public.employees(firstname, lastname, birthday, access_card_idaccess_card, email, telephone, department, function, address_idaddress)Values('" + fn + "', '" + ln +  "', '" + br +"', " + str((n+1)) + ", '" + email + "', '+42070279302" + str(n) + "', 'výroba', '" + str(selectFC()) +"', " + str((n+1)) + ");")

for n in range(5): #login
    print("INSERT INTO public.login(employees_iduser, login, password)Values("+ str((n+1)) + ", 'em0130" + str(n) +"', '"+str(generate(8))+"');")

for n in range(5): #event
    evb = "'" + str(random.randrange(1,29))+'.'+str(random.randrange(1,13))+'.'+str(random.randrange(2020,2022)) + " " + str(random.randrange(0,24))+':'+str(random.randrange(0,60)) + ':' + str(random.randrange(0, 60)) + "'"
    eve = "'" + str(random.randrange(1,29))+'.'+str(random.randrange(1,13))+'.'+str(random.randrange(2022,2024)) + " " + str(random.randrange(0,24))+':'+str(random.randrange(0,60)) + ':' + str(random.randrange(0, 60)) + "'"
    print("INSERT INTO public.events(eventb, evente, title, description)Values("+ evb + ", " + eve + ", 'Událost " + str(n) + "', '" + str(generatetext(64))+"');")

for n in range(5): #empHasevnt
    print("INSERT INTO public.employees_has_events(employees_iduser, events_idevents)Values("+str(random.randrange(1,6))+', '+str(random.randrange(1,6))+");")

for n in range(10): #msg
    tt = "'" + str(random.randrange(1,29))+'.'+str(random.randrange(1,13))+'.'+str(random.randrange(2020,2022)) + " " + str(random.randrange(0,24))+':'+str(random.randrange(0,60)) + ':' + str(random.randrange(0, 60)) + "'"
    print("INSERT INTO public.message(employees_iduser_from, employees_iduser_to, text, date)Values("+str(random.randrange(1,6))+', '+str(random.randrange(1,6))+ ", '" + str(generatetext(64)) + "', " + tt + ");")

for n in range(10): #note
    print("INSERT INTO public.note(employees_iduser, messtext)Values("+str(random.randrange(1,6))+", '" + str(generatetext(120)) + "');")

for n in range(5): #pay
    tt = "'" + str(random.randrange(1,29))+'.'+str(random.randrange(1,13))+'.'+str(random.randrange(2020,2022)) + " " + str(random.randrange(0,24))+':'+str(random.randrange(0,60)) + ':' + str(random.randrange(0, 60)) + "'"
    print("INSERT INTO public.pay(employees_iduser, grosswage, rewards, hoursworked, carriedout)Values("+str(random.randrange(1,6))+", " +str(random.randrange(32,57)*10000)+", " +str(random.randrange(2,5)*10000)+", "+str(random.randrange(40,150))+", "+ tt + ");")

for n in range(10): #accessHist
    tt = "'" + str(random.randrange(1,29))+'.'+str(random.randrange(1,13))+'.'+str(random.randrange(2020,2022)) + " " + str(random.randrange(0,24))+':'+str(random.randrange(0,60)) + ':' + str(random.randrange(0, 60)) + "'"
    print("INSERT INTO public.access_history(door_iddoor, access_card_idaccess_card, date)Values("+str(random.randrange(1,50))+', '+str(random.randrange(1,50))+ ", " + tt + ");")
